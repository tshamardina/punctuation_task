from numpy.random import seed
seed(50)
from tensorflow import set_random_seed
set_random_seed(50)

# Import required packages and dependencies
import numpy as np
import keras
import tensorflow as tf
from keras import backend as K
K.tensorflow_backend._get_available_gpus()
from keras.callbacks import TensorBoard, EarlyStopping, ModelCheckpoint
from keras.models import Model, load_model
from keras.layers import Activation
from keras.optimizers import Adam
from keras.layers import Embedding, Conv1D, Flatten, Dense, Dropout, LSTM, GRU, Bidirectional, TimeDistributed, \
Dropout, Input, concatenate, Reshape
# from tensorflow.keras.layers import Attention, GlobalAveragePooling1D
from keras import regularizers
from keras.layers.normalization import BatchNormalization

def set_callbacks(monitor, patience, dataset_name, save_best_only, batch_size):
    '''
    Descriptin:
        - Sets parameters for callbacks during training
        
    Args:
        - monitor: Metric to monitor during training for changes
        - patience: Number of epochs to keep training when there are no improvements
        - dataset_name: Name of dataset
        - save_best_only: Saves best training results
        - batch_size: Number of training data/labels per epoch
        
    Returns:
        - early_s: Early stopping callback
        - chkpt: Model checkpoint callback
        - tensor_b: Tensorboard callback
    '''
    early_s = EarlyStopping(monitor=monitor, patience=patience, verbose=1)
    chkpt = ModelCheckpoint(filepath='./models/'+dataset_name+'_model_my.h5',
                        monitor=monitor, save_best_only=save_best_only, verbose=1)
    tensor_b = TensorBoard(log_dir='./tf_logs/{}_model_my'.format(dataset_name, batch_size=batch_size, 
                        write_graph=False, histogram_freq=0))
    
    return early_s, chkpt, tensor_b

def create_model(max_seq_len, input_dim, output_dim, embed_weights, seq_len, trainable, drop_prob, lstm_hidden_2, 
                no_classes, model_type, lstm_hidden, filter_sizes, kernel_weight, bias, kernels, kernel_reg, cnn_activation,
                 cnn_padding, dense_activation, adam_lr, **kwargs):
    '''
    Description: 
        - Constructs and compiles the CNN+BiLSTM model
    
    Args:
        - max_seq_len: Maximum sequence length of each sequence, defines input shape (None, 128)
        - input_dim: Size of vocabulary, number of unique words
        - output_dim: Dimension of the dense embedding (128, 300)
        - embed_weights: Pre-trained weights extracted from Glove embeddings
        - seq_len: Length of each input sequence
        - trainable: If the pre-trained weights can be trained
        - drop_prob: Dropout rate 
        - lstm_hidden_2: Number of hidden units for the BiLSTM layer
        - no_classes: Number of classes to predict for 
        - model_type: Hybrid, Baseline or MGE
        
    **kwargs(Arguements used when creating the Hybrid and MGE models):
        - lstm_hidden: Number of hidden units in Dense layer after CNN
        - filter_sizes: Filter sizes for each CNN layer
        - kernel_size: Kernel sizes for each CNN layer
        - kernel_reg: Kernel regularization
        - kernel_initializer: Kernel weights initialization (Glorot)
        - bias_initializer: Bias initialization for CNN layers
        - activation: Activation function for Dense layer
        - padding: Padding type for CNN layers
        
    Return: 
        - model: Compiled model
    '''
    embed_input = Input(shape=(max_seq_len,))

    # Add embedding layer using weights from glove
    embed = Embedding(input_dim=input_dim, output_dim=output_dim, weights=[embed_weights],
                      input_length=seq_len, trainable=trainable)(embed_input) #104910 * 300
    
    # Add dropout to embedding layer
    embed = Dropout(rate=drop_prob)(embed)
    
    if model_type == 'baseline':
        blstm_outputs = Bidirectional(LSTM(lstm_hidden_2, return_sequences=True))(embed)
        blstm_outputs = Dropout(rate=drop_prob)(blstm_outputs)
        output = TimeDistributed(Dense(no_classes, activation='softmax'))(blstm_outputs)
    else:        
        cnn_outputs = []
        for i in range(len(filter_sizes)):
            # Add conv1d layer
            out_i = Conv1D(filters=filter_sizes[i], kernel_initializer=kernel_weight, bias_initializer=bias, 
                              kernel_size=kernels[i], kernel_regularizer=kernel_reg, activation=cnn_activation, 
                              padding=cnn_padding, strides=1)(embed)
            cnn_outputs.append(out_i)

        cnn_outputs = concatenate(cnn_outputs, axis=-1)
        cnn_outputs = Dropout(rate=drop_prob)(cnn_outputs)
        cnn_outputs = Reshape((-1, np.sum(filter_sizes)))(cnn_outputs)

        dense = Dense(lstm_hidden, activation=dense_activation)(cnn_outputs)
        dense = Dropout(rate=drop_prob)(dense)

        blstm_outputs = Bidirectional(LSTM(lstm_hidden_2, return_sequences=True))(dense)

        blstm_outputs = Dropout(rate=drop_prob)(blstm_outputs)

        output = TimeDistributed(Dense(no_classes, activation='softmax'))(blstm_outputs)

    model = Model(inputs=[embed_input], outputs=[output])
    model.compile(loss='categorical_crossentropy', optimizer=Adam(adam_lr), 
              metrics=['accuracy'])
    
    return model

def simple_gru(max_seq_len, input_dim, output_dim, embed_weights, seq_len, trainable, drop_prob, lstm_hidden_2, 
                no_classes, model_type, lstm_hidden, filter_sizes, kernel_weight, bias, kernels, kernel_reg, cnn_activation,
                 cnn_padding, dense_activation, adam_lr, **kwargs):

    embed_input = Input(shape=(max_seq_len,))

    # Add embedding layer using weights from glove
    embed = Embedding(input_dim=input_dim, output_dim=output_dim, weights=[embed_weights],
                      input_length=seq_len, trainable=trainable)(embed_input) #104910 * 300
    
    # Add dropout to embedding layer
#     embed = Dropout(rate=drop_prob)(embed)

    cnn_outputs = []
    for i in range(len(filter_sizes)):
        # Add conv1d layer
        out_i = Conv1D(filters=filter_sizes[i], kernel_initializer=kernel_weight, bias_initializer=bias, 
                          kernel_size=kernels[i], kernel_regularizer=kernel_reg, activation=cnn_activation, 
                          padding=cnn_padding, strides=1)(embed)
        cnn_outputs.append(out_i)

    cnn_outputs = concatenate(cnn_outputs, axis=-1)
#     cnn_outputs = Dropout(rate=drop_prob)(cnn_outputs)
    cnn_outputs = Reshape((-1, np.sum(filter_sizes)))(cnn_outputs)

    dense = Dense(lstm_hidden, activation=dense_activation)(cnn_outputs)
#     dense = Dropout(rate=drop_prob)(dense)

    blstm_outputs = Bidirectional(GRU(lstm_hidden_2, return_sequences=True))(dense)

#     blstm_outputs = Dropout(rate=drop_prob)(blstm_outputs)

    output = TimeDistributed(Dense(no_classes, activation='softmax'))(blstm_outputs)    

    model = Model(inputs=[embed_input], outputs=[output])
    model.compile(loss='categorical_crossentropy', optimizer=Adam(adam_lr), 
              metrics=['accuracy'])
    
    return model