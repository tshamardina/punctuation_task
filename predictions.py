import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

def plot_train_hist(model):
    '''
    Description:
        - Plots and prints monitored training metrics
        
    Args:
        - model: Trained model
    '''
    training_accuracy = model.history['acc']
    test_accuracy = model.history['val_acc']
    
    # Create count of the number of epochs
    epoch_count = range(1, len(training_accuracy) + 1)

    # Visualize accuracy history
    plt.plot(epoch_count, training_accuracy, 'r--')
    plt.plot(epoch_count, test_accuracy, 'b-')
    plt.legend(['Training Accuracy', 'Test Accuracy'])
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy Score')
    plt.show()

# def get_results(model, test_x, test_y, dataset_type): 
def get_results(model, test_x, test_y): 
    '''
    Description:
        - Generates Classification Reports and Confusion Matrices using trained models and their corresponding test
          data
          
    Args:
        - model: Trained model
        - test_x: Testing data
        - test_y: Testing labels (True labels)
        - dataset_type: Dataset type (Hybrid, Baseline or, MGE)
        
    Returns:
        - con_matrix: Confusion Matrix
        - class_rept_dict: Classification Report in dictionary form
    '''
#     if dataset_type == 'mge' or dataset_type == 'MGE':
#         target_names = ['Pad', 'NA', 'Period', 'Comma', 'Question', 'Exclaim', 'Ellipsis']
#     else:
#         target_names = ['Pad', 'NA', 'Comma', 'Period', 'Question', 'Exclaim', 'Ellipsis']   
        
    predict = model.predict(test_x)
    output_pred = [np.argmax(x, axis=1) for x in predict]
    output_pred = np.concatenate(output_pred, axis=0)

    y_ = [np.argmax(y, axis=1) for y in test_y]
    y_ = np.concatenate(y_, axis=0)
    
    con_matrix = confusion_matrix(y_true=y_, y_pred=output_pred)
    
#     class_rept = classification_report(y_true=y_, y_pred=output_pred, target_names=target_names)
    
    class_rept_dict = classification_report(y_true=y_, y_pred=output_pred, output_dict=True)  
    
#     return con_matrix, class_rept, class_rept_dict
    return con_matrix, class_rept_dict

def get_df(class_rept_dict, dataset_type):
    '''
    Description:
        - Converts the Classification Report dictionary into a Pandas DataFrame which can be used to display in a table format
        
    Args:
        - class_rept_dict: Dictionary of the Classification Report
        - dataset_type: Type of dataset the report was processed on
        
    Returns:
        - df: Converted dataframe with entries rounded to 2 decimal places
    '''
    precision = []
    recall = []
    f1 = []
    support = []

    data = {'precision': precision,
           'recall': recall,
           'f1-score': f1,
           'support': support}
    
    for idx, values in enumerate(class_rept_dict.items()):
        precision.append(values[1]['precision'])
        recall.append(values[1]['recall'])
        f1.append(values[1]['f1-score'])
        support.append(values[1]['support'])
    
    df = pd.DataFrame.from_dict(data)
    if dataset_type == 'MGE' or dataset_type == 'mge':
        df.rename(index={0:'Pad', 1:'NA', 2:'Period', 3:'Comma', 4:'Question', 5:'Exclaim', 6:'Ellipsis', 
                         7:'micro avg', 8:'macro avg', 9:'weighted avg'}, inplace=True)
    else:
        df.rename(index={0:'Pad', 1:'NA', 2:'Comma', 3:'Period', 4:'Question', 5:'Exclaim', 6:'Ellipsis',
                         7:'micro avg', 8:'macro avg', 9:'weighted avg'}, inplace=True)

    return df.round(2)
    
def make_prediction(model, test_seq, vocabs, labels, dataset_type, show_process=False):
    '''
    Description:
        - Generates a prediction using sample test data
        
    Args:
        - model: Trained model
        - test_seq: Sample test data sequence
        - vocabs: Words to integer mapping of dataset
        - labels: labels to integer mapping of dataset
        - dataset_type: Dataset type (Hybrid, Baseline or MGE)
        - show_process: Displays intermediate results prior to final (Default False)
    '''
    # Load a sample of test data
    test_data = test_seq

    # Restore tokenized test data back to normal sentence
    pred_x_seq = []
    for x in test_data:
        for value, index in vocabs.items():
            if x == index:
                pred_x_seq.append(value)

    # Get predicted output of test data (Make predictions)
    pred_expand = model.predict(np.expand_dims(test_data, axis=0))

    # Retrieve position of highest probability from predictions
    pred_y = []
    for y in pred_expand:
        pred_y.append(np.argmax(y, axis=1))

    # Restore tokenized labels
    pred_y_seq = []
    for x in pred_y:
        for y in x:
            for value, index in labels.items():
                if y == index:
                    pred_y_seq.append(value)

    # Restore punctuations and capitalization                
    combined = []
    if dataset_type == 'mge':
        for i in range(len(pred_x_seq)):
            if pred_y_seq[i] == '2':
                combined.append(str(pred_x_seq[i])+',')
            elif pred_y_seq[i] == '1':
                combined.append(str(pred_x_seq[i])+'.')
            elif pred_y_seq[i] == '3':
                combined.append(str(pred_x_seq[i])+'?')
            elif pred_y_seq[i] == '4':
                combined.append(str(pred_x_seq[i])+'!')
            elif pred_y_seq[i] == '5':
                combined.append(str(pred_x_seq[i])+'...')
            else:
                combined.append(str(pred_x_seq[i]))
    else:
        for i in range(len(pred_x_seq)):
            if pred_y_seq[i] == '<comma>':
                combined.append(str(pred_x_seq[i])+',')
            elif pred_y_seq[i] == '<period>':
                combined.append(str(pred_x_seq[i])+'.')
            elif pred_y_seq[i] == '<question>':
                combined.append(str(pred_x_seq[i])+'?')
            elif pred_y_seq[i] == '<exclaim>':
                combined.append(str(pred_x_seq[i])+'!')
            elif pred_y_seq[i] == '<3-dots>':
                combined.append(str(pred_x_seq[i])+'...')
            elif pred_y_seq[i] == '<dash>':
                combined.append(str(pred_x_seq[i])+'–')
            elif pred_y_seq[i] == '<colon>':
                combined.append(str(pred_x_seq[i])+':')
            elif pred_y_seq[i] == '<semicolon>':
                combined.append(str(pred_x_seq[i])+';')
#             elif pred_y_seq[i] == '<quotes>':
#                 combined.append(str(pred_x_seq[i])+'"')
#             elif pred_y_seq[i] == '<bra>':
#                 combined.append(str(pred_x_seq[i])+'(')
#             elif pred_y_seq[i] == '<ket>':
#                 combined.append(str(pred_x_seq[i])+')')
            else:
                combined.append(str(pred_x_seq[i]))

    for i in range(len(combined)):
        if '.' in combined[i] or '?' in combined[i] or '!' in combined[i] or '...' in combined[i]:
            combined[i+1] = combined[i+1].capitalize()
        elif combined[i] == 'i':
            combined[i] = combined[i].capitalize()
        else:
            continue

    # Join predicted words back into a sequence
    combined = ' '.join(combined)
    combined = combined.replace('<pad>', '')
    
    if show_process:
        print('Predictions Index:')
        print(pred_y)
        print('\n')
        print('Prediction sequence:')
        print(' '.join(pred_x_seq))
        print('\n')
        print('Prediction output:')
        print(' '.join(pred_y_seq))
        print('\n')
        print('Combined prediction:')
        print(combined[0].capitalize()+combined[1:].replace('ive', "I've"))
    else:
        print('Combined prediction:')
        print(combined[0].capitalize()+combined[1:].replace('ive', "I've"))
        
    return combined[0].capitalize()+combined[1:].replace('ive', "I've")