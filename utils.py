import string
from nltk.tokenize import wordpunct_tokenize


def check_one(reference, hypothesis):
    correct = 0
    incorrect = 0
    ref = wordpunct_tokenize(reference)
    hyp = wordpunct_tokenize(hypothesis)
    ref_i, hyp_i = 0, 0
    punct_places = 0
    while ref_i < len(ref) and hyp_i < len(hyp):
        need_punct_check_ref = False
        need_punct_check_hyp = False
        cur_ref = ref[ref_i]
        if cur_ref in string.punctuation:
            need_punct_check_ref = True
            punct_places += 1
        cur_hyp = hyp[hyp_i]
        if cur_hyp in string.punctuation:
            need_punct_check_hyp = True
        if need_punct_check_ref and need_punct_check_hyp:
            if cur_ref == cur_hyp:
                correct += 1
            else:
                incorrect += 1
            ref_i += 1
            hyp_i += 1
            continue

        if need_punct_check_ref and not need_punct_check_hyp:
            incorrect += 1
            ref_i += 1
            continue

        if not need_punct_check_ref and need_punct_check_hyp:
            incorrect += 1
            hyp_i += 1
            continue

        assert cur_hyp == cur_ref, "The phrases are inconsistent!"
        ref_i += 1
        hyp_i += 1

    return correct/punct_places - incorrect/(2 * len(reference))


gold_str = "Начиная жизнеописание героя моего, Алексея Федоровича Карамазова, нахожусь в некотором недоумении. " \
           "А именно: хотя я и называю Алексея Федоровича моим героем, но, однако, сам знаю, что человек он " \
           "отнюдь не великий, а посему и предвижу неизбежные вопросы вроде таковых: чем же замечателен ваш " \
           "Алексей Федорович, что вы выбрали его своим героем?"
test_str = "Начиная жизнеописание героя моего, Алексея Федоровича Карамазова нахожусь в некотором недоумении. " \
           "А именно: хотя я и называю Алексея Федоровича моим героем, но однако, сам знаю, что человек он " \
           "отнюдь не великий, а посему, и предвижу неизбежные вопросы вроде таковых - чем же замечателен ваш " \
           "Алексей Федорович, что вы выбрали его своим героем?"


if __name__ == "__main__":
    print("Quality: {:.2f}%".format(check_one(gold_str, test_str) * 100))
    # Quality: 74.42%
